willow (1.6.2-3) unstable; urgency=medium

  * Team upload.
  * Fix flaky autopkgtest (Closes: #1055391)

 -- Stephan Lachnit <stephanlachnit@debian.org>  Sat, 25 Nov 2023 14:53:07 +0100

willow (1.6.2-2) unstable; urgency=medium

  * Team upload.
  * Fix flaky autopkgtest

 -- Stephan Lachnit <stephanlachnit@debian.org>  Thu, 26 Oct 2023 18:18:40 +0200

willow (1.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.6.2 (Closes: #1042117)
  * Removed doc packages due to missing dependencies

 -- Stephan Lachnit <stephanlachnit@debian.org>  Wed, 25 Oct 2023 14:22:37 +0200

willow (1.4-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-willow-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 08 Dec 2022 01:17:15 +0000

willow (1.4-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 22:03:27 -0400

willow (1.4-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1.4 (Closes: #951990)
  * Run tests with runtests.py
  * Add upstream/metadata
  * standards version: 4.5.0, compat version:13
  * Add "Rules-Requires-Root:no"
  * Run wrap-and-sort

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 30 Jul 2020 01:21:25 +0530

willow (1.1-4) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Fix doc-base.

 -- Ondřej Nový <onovy@debian.org>  Sun, 04 Aug 2019 19:46:42 +0200

willow (1.1-3) unstable; urgency=medium

  * Fix python-package-depends-on-package-from-other-python-variant
  * Bump compat level from 9 to 11
  * Bump Standards-Version from 4.1.2 to 4.1.3 (no change required)

 -- Christopher Hoskin <mans0954@debian.org>  Wed, 24 Jan 2018 22:17:10 +0000

willow (1.1-2) unstable; urgency=medium

  * Add Python 3 package
  * Update Standards-Version from 4.1.1 to 4.1.2 (no change required)

 -- Christopher Hoskin <mans0954@debian.org>  Wed, 06 Dec 2017 07:00:25 +0000

willow (1.1-1) unstable; urgency=medium

  * Remove repack script, upstream is now DFSG compliant
  * Update debian/copyright
  * New upstream release (1.0)
  * Build-Depend on python3-sphinx rather than python-sphinx
  * Get tests running (currently failing)
  * New upstream release (1.1)
  * Remove trailing whitespace from debian/control and debian/changelog
  * Increase Standards-Version from 3.9.8 to 4.1.1 (no change required)
  * Update debian/copyright

 -- Christopher Hoskin <mans0954@debian.org>  Tue, 05 Dec 2017 23:14:41 +0000

willow (0.4+dfsg-1) unstable; urgency=medium

  * New upstream release (0.4)
  * Update debian/copyright
  * Remove unnecessary dependency versioning and fields from debian/control

 -- Christopher Hoskin <mans0954@debian.org>  Tue, 28 Feb 2017 07:32:32 +0000

willow (0.3.1+dfsg-1) unstable; urgency=medium

  * Switch maintainer to my Debian identity
  * Add repack script to remove non-DFSG compliant ICC profiles from rotated
    images.
  * Fix "Embedded ICC profiles violate DFSG" use tarball repacked by script
    (Closes: #852943)
  * Add patch to adjust orientation test tollerances

 -- Christopher Hoskin <mans0954@debian.org>  Sun, 29 Jan 2017 16:52:36 +0000

willow (0.3.1-1) unstable; urgency=low

  * Initial packaging, based on stdeb 0.8.5 output. Closes: 837471
  * add the tag format in debian/.git-dpm
  * Patch to correct Willow version
  * Add Vcs fields to debian/control
  * Minor fixes at request of sponsor

 -- Christopher Hoskin <christopher.hoskin@gmail.com>  Wed, 21 Dec 2016 20:55:20 +0000
